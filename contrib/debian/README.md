
Debian
====================
This directory contains files used to package globaxcoind/globaxcoin-qt
for Debian-based Linux systems. If you compile globaxcoind/globaxcoin-qt yourself, there are some useful files here.

## globaxcoin: URI support ##


globaxcoin-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install globaxcoin-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your globaxcoin-qt binary to `/usr/bin`
and the `../../share/pixmaps/globaxcoin128.png` to `/usr/share/pixmaps`

globaxcoin-qt.protocol (KDE)

