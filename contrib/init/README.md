Sample configuration files for:

SystemD: globaxcoind.service
Upstart: globaxcoind.conf
OpenRC:  globaxcoind.openrc
         globaxcoind.openrcconf
CentOS:  globaxcoind.init
OS X:    org.globaxcoin.globaxcoind.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
